

%q7 b
A = [power(10, -20) 1; 1 1];
%LU decomposition without permutation
[L, U] = lu(sparse(A),0);
L = full(L);
U = full(U);
printMatrix("L", L);
printMatrix("U",U);
printMatrix("L*U", L*U);


%q7 d
A = [power(10, -20) 1; 1 1];
%LU decomposition with permutation
[L, U, P] = lu(A);
L = full(L);
U = full(U);
P = full(P);
printMatrix("P", P);

printMatrix("L", L);
printMatrix("U",U);

printMatrix("P*A", P*A);
printMatrix("L*U", L*U);







function [] = printMatrix(name, M)
    fprintf("%s\n", name)
    fprintf('%d ', M(1,1))
    fprintf('%d', M(1,2))
    fprintf('\n%d', M(2,1))
    fprintf(' %d\n\n', M(2,2))
end