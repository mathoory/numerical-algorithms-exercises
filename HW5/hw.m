clearvars; close all;
% Loading the original image
X = imread('./ffhq0_256/455.jpg');
X = double(X)/256; % values in [0,1]

% Loading the blur matrix
H_struct = load('H.mat');
H = H_struct.H;

[m, n, c] = size(X);
x_vector_original = reshape(X, m * n * c, 1);
y_vector = H * x_vector_original;
Y = reshape(y_vector, m, n, c);

figure; imshow(X); title('Original Image');
figure; imshow(Y); title('Blurred Image');

%Hx=y <=> Ax=b

% init U to random normalized vector
u = randn(m * n * c, 1);
u = u / norm(u);

% Calculate T
U = triu(H,1);  % upper diagonal
DL = tril(H);

a_array = zeros(1, 300);


for k=1:300
    curr =  U*u;
    curr = -DL\curr;
    a_array(k) = transpose(u)*curr;

    u = curr;
    u = u / norm(u);
end

figure; plot(a_array); title("a(k)");

x_vector_gs = zeros(m * n * c, 1);

%Gaus Hayde
err_array = zeros(1, 100);

for k=1:100
    x_vector_gs = tril(H)\(y_vector-U*x_vector_gs);
    err_array(k)=(1/(m*n*c))*norm(y_vector-H*x_vector_gs);
end

figure; plot(err_array); title("err");

X_reconstructed = reshape(x_vector_gs, m, n, c);
figure; imshow(X_reconstructed); title('Restored Image');

