%% ====================== Prepare New Test Set ======================
load('mnist.mat');

N = 4000;
digit1 = 0;

imagesPerDigit1 = training.images(:,:,training.labels == digit1);
imagesNonDigit1 = training.images(:,:,training.labels ~= digit1);

%% ======================= Create A, b ============================
A_all = zeros(10*N,28^2);
b_all = zeros(10*N,1);

for i=1:N
    A_all(i*10-9,:) = reshape(imagesPerDigit1(:,:,i),1,28*28);
    b_all(i*10-9)   = +1;
   
    for j=1:9
         A_all(i*10-9+j,:)   = reshape(imagesNonDigit1(:,:,(i-1)*9+j),1,28*28);
         b_all(i*10-9+j)     = -1; 
    end
end
A_all = [A_all, ones(10*N,1)];

%% ========================= Solve LS ==============================
A_train = A_all(1:5*N,:); 
b_train = b_all(1:5*N); 

x=pinv(A_train)*b_train; 

A_test = A_all(5*N+1:10*N,:); 
b_test = b_all(5*N+1:10*N); 

%% ===================== Check Performance ===========================

predC = sign(A_train*x); 
trueC = b_train; 
disp('Train Error:'); 
acc=mean(predC == trueC)*100;
disp(['Accuracy=',num2str(acc),'% (',num2str((1-acc/100)*5*N),' wrong examples)']); 

predC = sign(A_test*x); 
trueC = b_test; 
disp('Test Error:'); 
acc=mean(predC == trueC)*100;
disp(['Accuracy=',num2str(acc),'% (',num2str((1-acc/100)*5*N),' wrong examples)']); 

%% ================= Show the Problematric Images ====================

error = find(predC~=trueC); 
for k=1:1:length(error)
    figure(2);
    imagesc(reshape(A_test(error(k),1:28^2),[28,28]));
    colormap(gray(256))
    axis image; axis off; 
    title(['problematic digit number ',num2str(k),' :',num2str(A_test(error(k),:)*x)]); 
    pause;  
end