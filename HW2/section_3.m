%% ====================== Prepare New Test Set ======================
load('mnist.mat');

num_images = test.count;
new_test_images = shiftdim(test.images, 2);
A_new_test = reshape(new_test_images,num_images,28*28);
A_new_test = [A_new_test, ones(num_images,1)];
true_labels = test.labels;

%% ============================ Predict ==============================
UNCLASSIFIED = -1;
pred = UNCLASSIFIED * ones(num_images, 1);

outer_unknown = 0;
weights = [];
for k=0:1:9
    weights = [weights, getWeights(k)];
end

for k=1:1:num_images
    [temp, outer_unknown] = classify(A_new_test(k,:), weights,outer_unknown);
    pred(k) = temp;
    %disp(['pred(k):', num2str(pred(k)), 'true_labels(k)', num2str(true_labels(k))]);
  
end



%% =========================== Evaluate ==============================
acc = mean(pred == true_labels)*100;
disp(['Accuracy=',num2str(acc),'% (',num2str((1-acc/100)*num_images),' wrong examples)']); 
disp(['Unknown: ',num2str(outer_unknown)]);

%% ================= Show the Problematric Images ====================
error = find(pred~=true_labels); 
for k=1:1:5
    figure(2);
    imagesc(reshape(A_new_test(error(k),1:28^2),[28,28]));
    colormap(gray(256))
    axis image; axis off;  
    pause;  
end

function [digit, inner_unknown] = classify(img, weights, inner_unknown)
    %% ===================== Check Performance ===========================
    predC = sign(img*weights); %predC is a vector of size 10 that contains the classified results

    digit = find(predC == 1);
    
    disp(size(digit));

    if(size(digit,2) ~= 1)
        %disp('Not sure');
        inner_unknown = inner_unknown+1;

        if(isempty(digit))
            digit = -1;
            return;
        end
    end

    
    digit = digit(1);
    digit = digit - 1;
end


function weights = getWeights(digit)
    %% ====================== Prepare New Test Set ======================
        load('mnist.mat');
        N = 4000;

        imagesPerDigit1 = training.images(:,:,training.labels == digit);
        imagesNonDigit1 = training.images(:,:,training.labels ~= digit);

        %% ======================= Create A, b ============================
        A_all = zeros(10*N,28^2);
        b_all = zeros(10*N,1);

        for i=1:N
            A_all(i*10-9,:) = reshape(imagesPerDigit1(:,:,i),1,28*28);
            b_all(i*10-9)   = +1;

            for j=1:9
                 A_all(i*10-9+j,:)   = reshape(imagesNonDigit1(:,:,(i-1)*9+j),1,28*28);
                 b_all(i*10-9+j)     = -1; 
            end
        end
        A_all = [A_all, ones(10*N,1)];

        %% ========================= Solve LS ==============================
        weights=pinv(A_all)*b_all; 
end